# Kong : API Management pour les devs

## 1 - Configuration de Kong

Créer le network Docker qu'on va appeler `kong-net` :

```sh
docker network create kong-net
```

Nous allons lier tous les conteneurs Docker à ce network.

Créer une base de données PostgreSQL qu'on va appeler `kong-database` :

```sh
docker run -d --name kong-database \
  --network=kong-net \
  -p 5432:5432 \
  -e "POSTGRES_DB=kong" \
  -e "POSTGRES_USER=kong-user" \
  -e "POSTGRES_PASSWORD=kong-password" \
  postgres:9.6
```

Construire l'image locale Kong avec sa configuration spécifique avec la commande suivante :

```sh
docker build -t kong-local .
```

Cela copie le fichier de configuration `./infra/kong.conf` de ce repository dans le répertoire `etc/` du conteneur.

Le serveur Kong ne peut pas être exposé en HTTPS sur l'environnement local, donc nous configurons le serveur pour
accepter les requêtes HTTPS qui ont été interceptées par un proxy (ou un load balancer) (`accept_http_if_already_terminated=true`) et contenant le
header `x-forwarded-proto: https`.

Préparer la base de données PostgreSQL (jouer les migrations) en utilisant un conteneur éphémère, avec
l'image `kong-local` précédemment créée :

```sh
docker run --rm \
  --network=kong-net \
  -e "KONG_DATABASE=postgres" \
  -e "KONG_PG_HOST=kong-database" \
  -e "KONG_PG_USER=kong-user" \
  -e "KONG_PG_PASSWORD=kong-password" \
  kong-local:latest kong migrations bootstrap
```

Créer et démarrer le conteneur Kong avec l'image `kong-local` :

```sh
docker run -d --name kong \
  --network=kong-net \
  -e "KONG_DATABASE=postgres" \
  -e "KONG_PG_HOST=kong-database" \
  -e "KONG_PG_USER=kong-user" \
  -e "KONG_PG_PASSWORD=kong-password" \
  -e "KONG_PROXY_ACCESS_LOG=/dev/stdout" \
  -e "KONG_ADMIN_ACCESS_LOG=/dev/stdout" \
  -e "KONG_PROXY_ERROR_LOG=/dev/stderr" \
  -e "KONG_ADMIN_ERROR_LOG=/dev/stderr" \
  -e "KONG_ADMIN_LISTEN=0.0.0.0:8001, 0.0.0.0:8444 ssl" \
  -p 8000:8000 \
  -p 8443:8443 \
  -p 127.0.0.1:8001:8001 \
  -p 127.0.0.1:8444:8444 \
  kong-local:latest
```

Vérifiez que les containers sont bien démarrés (UP) :

```sh
# Appeler l'URL d'admin Docker
curl -i http://localhost:8001/

# Voir la liste des conteneurs (actifs ou non)
docker ps -a

# Voir les logs d'un conteneur en particulier
docker logs <id_conteneur>
```

Avec la configuration ci-dessus :

- Les API d'administration de Kong sont sur le port 8001. (Kong se configure entièrement via API.)
- L'API (API Pets) qu'on cherche à consommer via Kong se trouve sur le port 8000

## 2 - Configuration d'api-device et d'api-pets

### Routing

Supposons que nous avons deux applications que nous souhaitons manager via Kong :

- Notre API "api-pets"
- Une API appelée "api-devices"

Nous allons déclarer un "service" Kong pour chacune de ces applications et leur associer des règles de routing. (Kong va
se charger de rediriger les appels vers la bonne application sous-jacente.)

Pour api-devices :

```sh
# Créer un service appelé "api-devices"
# Cela va stocker le host "api-devices" et le port 8080
curl -i -X POST \
  --url http://localhost:8001/services/ \
  --data 'name=api-devices' \
  --data 'url=http://api-devices:8080'

# Créer une route pour ce service (ajout d'un host) pour que Kong agisse comme un proxy
curl -i -X POST \
  --url http://localhost:8001/services/api-devices/routes \
  --data 'hosts[]=api-devices'
```

Pour api-pets :

```sh
# Créer un service appelé "api-pets"
# Cela va stocker le host "api-pets" et le port 8080
curl -i -X POST \
  --url http://localhost:8001/services/ \
  --data 'name=api-pets' \
  --data 'url=http://api-pets:8080'

# Créer une route pour ce service (ajout d'un host) pour que Kong agisse comme un proxy
curl -i -X POST \
  --url http://localhost:8001/services/api-pets/routes \
  --data 'hosts[]=api-pets'
```

Vérifier que ces paramétrages ont bien fonctionné et que les deux applications sont bien accessibles via Kong (port
8000) :

/!\ Important : Il faut avoir préalablement démarré api-pets via Docker comme indiqué dans
le [README de la partie api-pets](../../api-pets/README.md)).

```sh
# API Devices
# TODO: Créer une API Devices
curl -i -X GET \
  --url http://localhost:8000/healthcheck \
  --header 'host: api-devices'

# API Pets
curl -i -X GET  \
  --url http://localhost:8000/v2/pets?status=available \
  --header 'host: api-pets'
```

## 3 - Protéger l'application api-pets avec une API Key

Kong met à disposition un plugin pour mettre en place une API Key,
appelé "[Key Authentication](https://docs.konghq.com/hub/kong-inc/key-auth/)" (key-auth).

```sh
# Ajouter le plugin "key-auth" au service "api-pets"
# Nous configurons pour que le header se nomme "x-api-key"
curl -i -X POST \
  --url http://localhost:8001/services/api-pets/plugins \
  --data "name=key-auth" \
  --data "config.key_names=x-api-key"
```

Une requête `GET http://localhost:8000/v2/pets?status=available` renvoie désormais une erreur 401 avec le message "No
API key found in request", car la clé n'a pas été précisée en header. L'API est protégée !

Pour pouvoir utiliser l'API, il faut que chaque consommateur possède sa propre clé qui lui est propre. Il faut donc
commencer par renseigner des consommateurs d'API !

```sh
# Créer un consommateur
curl -i -X POST \
  --url http://localhost:8001/consumers/ \
  --data "username=bill.gates"

# Ajouter le plugin key-auth à ce consommateur et auto-génère une clé.
curl -i -X POST \
  --url http://localhost:8001/consumers/bill.gates/key-auth
```

La clé d'API pour ce consommateur se trouve dans le champ "key". Ex :

```json
{
  "next": null,
  "data": [
    {
      "key": "DukYaBh1xkfI90hyiHqvBpHpXKefYSIW",
      "consumer": {
        "id": "aea88bf2-524a-4ae1-a49a-b28200290445"
      },
      // ...
    }
  ]
}
```

Vérifier que l'api-pets est de nouveau accessible avec cette clé d'API, en la précisant dans le header `x-api-key` :

```sh
curl -i -X GET \
  --url http://localhost:8000/v2/pets?status=available \
  --header 'host: api-pets' \
  --header 'x-api-key: DukYaBh1xkfI90hyiHqvBpHpXKefYSIW'
```

La clé d'API fonctionne et permet d'accéder à l'API !

Avant de passer à la suite, pour éviter les conflits, il faut penser à retirer la clé d'API :

```sh
# Récupérer l'ID du plugin 
curl -i -X GET http://localhost:8001/services/api-pets/plugins/

# Supprimer le plugin en renseignant son ID
curl -X DELETE http://localhost:8001/plugins/<plugin-key-auth-id>
```

## 4 - Protéger l'application api-pets avec Oauth2

Kong met aussi à disposition un plugin pour
Oauth2 : "[OAuth 2.0 Authentication](https://docs.konghq.com/hub/kong-inc/oauth2/)".

### a - Flow Implicit Grant

Nous allons commencer par mettre en place un flow Oauth2 simple : **Implicit Grant**. Ce flow à la base utilisé pour les applications web est aujourd'hui déprécié, mais il reste un bon exercice pour découvrir Oauth2.

Configurer le plugin avec l'option `enable_implicit_grant` :

```sh
# Ajouter le plugin "oauth2" au service "api-pets"
# Nous configurons les scopes "read:pets" et "write:pets"
curl -i -X POST http://localhost:8001/services/api-pets/plugins \
  --data 'name=oauth2' \
  --data 'config.scopes=read:pets' \
  --data 'config.scopes=write:pets' \
  --data 'config.accept_http_if_already_terminated=true' \
  --data 'config.mandatory_scope=true' \
  --data 'config.enable_implicit_grant=true'
```

Penser à noter la valeur de `config.provision_key`, ce sera utile plus loin.

Une requête `GET http://localhost:8000/v2/pets?status=available` renvoie désormais une erreur 401 avec le message "The
access token is missing", car aucun access token n'a été précisée en header. L'API est protégée !

Nous créons une application Oauth2 pour le consommateur `bill.gates` créé plus haut, en précisant un nom et des URIs
autorisées :

```sh
curl -i -X POST http://localhost:8001/consumers/bill.gates/oauth2 \
  --data 'name=application-front-bill-gates' \
  --data 'redirect_uris=http://localhost:4200/bill-gates-website/post-login'
```

/!\ Aucune application ne répond sur le port 4200 dans notre exercice : cette URL n'est renseignée que pour l'exemple.
Dans la réalité, il s'agira de l'URL du site du consommateur d'API, sur laquelle il recevra l'access token.

La commande précédente génère automatiquement des credentials (`client_id` et `client_secret`) qui vont servir à
l'authentification. Il est possible à tout le moment de retrouver les informations Oauth d'un consommateur avec les
endpoints suivants :

```sh
# Ces endpoints sont équivalents

curl -i -X GET http://localhost:8001/consumers/<username-consumer>/oauth2

curl -i -X GET http://localhost:8001/oauth2?client_id=<client-id>
```

Résultat :

```json
{
  "next": null,
  "data": [
    {
      "name": "application-front-bill-gates",
      "client_id": "06E1XrUwkj2pRh8H44hSH7YWOkfXUamr",
      "client_secret": "nJepmHLXhB46j5rtg3PHzFoYDVr1hclo",
      "redirect_uris": [
        "http://localhost:4200/bill-gates-website/post-login"
      ],
      // ...
    }
  ]
}
```

Démarrer la danse Oauth2 d'authentification en demandant un token :

```sh
curl -i -X POST http://localhost:8000/oauth2/authorize \
  --header 'host: api-pets' \
  --header 'x-forwarded-proto: https' \
  --data 'response_type=token' \
  --data 'scope=read:pets' \
  --data 'authenticated_userid=jean.paul.admin' \
  --data 'client_id=<votre-client-id>' \
  --data 'provision_key=<provision-key-du-service>'
```

Cela retourne une `redirect_uri` qui doit commencer par celle renseignée plus
haut (http://localhost:4200/bill-gates-website/post-login) et qui contient surtout un fragment `access_token` qui va
servir à s'authentifier !

Décomposition des paramètres de cette URI :

- `#access_token=FmpzEPK7Fjf6Z923kGr8s2r6KrzZjKTF` : Le token d'authentification
- `&expires_in=7200` : La durée d'expiration en secondes
- `&token_type=bearer` :  Le type de token

/!\ Comme indiqué précédemment, le serveur http://localhost:4200 n'est volontairement pas accessible. Tout ce qui nous
intéresse c'est l'access token.

Vérifier que l'api-pets est de nouveau accessible avec cet access token, en le précisant dans le
header `authorization` :

```sh
curl -i -X GET \
  --url http://localhost:8000/v2/pets?status=available \
  --header 'host: api-pets' \
  --header 'authorization: Bearer FmpzEPK7Fjf6Z923kGr8s2r6KrzZjKTF'
```

Le flow Oauth2 Implicit Grant fonctionne et permet à un service externe (http://localhost:4200) d'accéder à l'API en
toute sécurité !

A noter qu'il est possible pour un admin de consulter tous les access tokens générés :

```sh
curl -i -X GET http://localhost:8001/oauth2_tokens
```

### b - Flow Authorization Code

C'est le flow traditionnel d'Oauth2, qui est incontournable. Il est plus complexe qu'Implicit Grant.

Pour le faire fonctionner, il faut que notre serveur d'identité propose une page de login. Nous allons pour cela lancer une autre application, qui se trouve dans le dossier `infra/kong/oauth2-login`.

Lancer le formulaire de login d'Oauth2 :

```sh
# Changer de dossier
cd oauth2-login

# Builder l'image
docker build -t oauth2-kong-login .

# Démarrer le conteneur
docker run -d --name oauth2-kong-login -p 3000:3000 \
  --network kong-net \
  -e "PROVISION_KEY=<provision-key-du-service>" \
  -e "SERVICE_HOST=kong" \
  -e "KONG_ADMIN=http://localhost:8001" \
  -e "KONG_API=http://localhost:8000" \
  -e "API_PATH=/" \
  -e "SCOPES={\"read:pets\":\"Cela permet de consulter la liste de vos animaux\"}" \
  oauth2-kong-login:latest
```

Le formulaire de login se trouve désormais sur l'URL http://localhost:3000/. Pour demander un token, il faut se rendre sur l'URL suivante depuis un navigateur (après avoir rempli le paramètre `client_id`) : 

`http://localhost:3000/authorize?client_id=<votre-client-id>&response_type=token&scope=read%3Apets`

Il faut se loguer avec les identifiants suivants (codés dans `oauth2-login/app.js`) :
- Adresse e-mail : test@test.com
- Mot de passe : iRahnguh4tieghei

Cela permet d'appréhender les notions de scopes et de délégation de droits.

## En cas d'erreur

### Problème "DNS resolution failed" ou "Connection refused while connecting to upstream"

Vérifiez que vous avez bien démarré api-pets dans Docker et non via `npm run start`.

### Problème "No API key found in request" lors du chapitre Oauth2

Il faut penser à supprimer le plugin `key-auth` avec :

```sh
curl -X DELETE http://localhost:8001/consumers/bill.gates/key-auth/<key-auth-id>
```
